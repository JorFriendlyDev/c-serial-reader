/*  Schema is makkelijk in te vullen aan de hand van onderstaande pin layout. Hiervoor moet je even een overzicht opzoeken
 *   van de 4 digit 7 segment pin layouts, aan de hand van deze de A,B,C,e e.d. pin variablen moet je jouw breadbord vullen. 
 */


const int led1 = A0;
const int led2 = A1;
const int led3 = A2;
const int buzzer = A3;
const int pinA = 2;
const int pinB = 3;
const int pinC = 4;
const int pinD = 5;
const int pinE = 6;
const int pinF = 7;
const int pinG = 8;
const int DP = 9;
const int D1 = 10;
const int D2 = 11;
const int D3 = 12;
const int D4 = 13;

bool showDegrees = false;
bool stringComplete = false;
String inputString = ""; 
String commandString = ""; 
int digit1 = 13;
int digit2 = 13;
int digit3 = 13;
int digit4 = 13;

void setup() {
  Serial.begin(9600);
  pinMode(pinA, OUTPUT);
  pinMode(pinB, OUTPUT);
  pinMode(pinC, OUTPUT);
  pinMode(pinD, OUTPUT);
  pinMode(pinE, OUTPUT);
  pinMode(pinF, OUTPUT);
  pinMode(pinG, OUTPUT);
  pinMode(DP, OUTPUT);
  pinMode(D1, OUTPUT);  
  pinMode(D2, OUTPUT);  
  pinMode(D3, OUTPUT);  
  pinMode(D4, OUTPUT);  
}

void loop() {
  
  writeOnLCD(1,digit1, false);
  writeOnLCD(2,digit2, showDegrees);
  writeOnLCD(3,digit3, false);
  writeOnLCD(4,digit4, false);
  
  if(stringComplete == true)
  {
    stringComplete = false;
    getCommand();
    
    if(commandString.equals("LED1"))
    {
        boolean LedState = getLedState();
        if(LedState == true)
        {
          turnLedOn(led1);
        }else
        {
          turnLedOff(led1);
        }  
    }
    else if(commandString.equals("LED2"))
    {
        boolean LedState = getLedState();
        if(LedState == true) 
        {
          turnLedOn(led2);
        }else
        {
          turnLedOff(led2);
        }
    }
    else if(commandString.equals("LED3"))
    {
        boolean LedState = getLedState();
        if(LedState == true)
        {
          turnLedOn(led3);
        }else
        {
          turnLedOff(led3);
        }
    }
    else if(commandString.equals("TEMC"))
    {
      showDegrees = true;
      setTemprature();
      digit4 = 10;
    }
    else if(commandString.equals("TEMF"))
    {
      showDegrees = true;
      setTemprature();
      digit4 = 11;
    }
    else if(commandString.equals("NUMB"))
    {
      showDegrees = false;
      setNumber();
    }
    else if(commandString.equals("STOP"))
    {
      showDegrees = false;
      digit1 = 13;
      digit2 = 13;
      digit3 = 13;
      digit4 = 13;
      turnLedOff(led1);
      turnLedOff(led2);
      turnLedOff(led3);
    }
 
    inputString = "";
  }
}

void turnLedOn(int pin)
{
  analogWrite(pin, 155);
}

void turnLedOff(int pin)
{
  analogWrite(pin, 0);
}

boolean getLedState()
{
  boolean state = false;
  if(inputString.substring(5,7).equals("ON"))
  {
    state = true;
  }else
  {
    state = false;
  }
  return state;
}

void setTemprature()
{
   digit1 = inputString.substring(5,6).toInt();
   digit2 = inputString.substring(6,7).toInt();
   digit3 = inputString.substring(8,9).toInt();
}

void getCommand()
{
  if(inputString.length()>0)
  {
    commandString = inputString.substring(1,5);
  }
}

void setNumber()
{
  digit1 = inputString.substring(5,6).toInt();
  digit2 = inputString.substring(6,7).toInt();
  digit3 = inputString.substring(7,8).toInt();
  digit4 = inputString.substring(8,9).toInt();
  delay(1);
}

void writeOnLCD(int digit, int number,bool decimal)
{
  digitalWrite(D1, HIGH);
  digitalWrite(D2, HIGH);
  digitalWrite(D3, HIGH);
  digitalWrite(D4, HIGH);
  switch(digit)
  {
    case 1:
      digitalWrite(D1, LOW);
      break;
    case 2:
      digitalWrite(D2, LOW);
      break;
    case 3:
      digitalWrite(D3, LOW);
      break;
    case 4:
      digitalWrite(D4, LOW);
      break;
    default:
      break;
  }

  digitalWrite(pinA, LOW);
  digitalWrite(pinB, LOW);
  digitalWrite(pinC, LOW);
  digitalWrite(pinD, LOW);
  digitalWrite(pinE, LOW);
  digitalWrite(pinF, LOW);
  digitalWrite(pinG, LOW);
  if(decimal)
  {
    digitalWrite(DP, HIGH);
  }
  else
  {
    digitalWrite(DP,LOW);
  }
  
  switch(number)
  {
    case 0:
      //0
      digitalWrite(pinA, HIGH);
      digitalWrite(pinB, HIGH);
      digitalWrite(pinC, HIGH);
      digitalWrite(pinD, HIGH);
      digitalWrite(pinE, HIGH);
      digitalWrite(pinF, HIGH);
      delay(1); 
      break;
    case 1:
      //1
      digitalWrite(pinB, HIGH);
      digitalWrite(pinC, HIGH);
      delay(1); 
      break;
    case 2:
      digitalWrite(pinA, HIGH);   
      digitalWrite(pinB, HIGH);     
      digitalWrite(pinD, HIGH);   
      digitalWrite(pinE, HIGH);     
      digitalWrite(pinG, HIGH);       
      delay(1);               
      break;
    case 3:
      digitalWrite(pinA, HIGH);   
      digitalWrite(pinB, HIGH);   
      digitalWrite(pinC, HIGH);   
      digitalWrite(pinD, HIGH);     
      digitalWrite(pinG, HIGH);       
      delay(1); 
      break;
    case 4:  
      digitalWrite(pinB, HIGH);   
      digitalWrite(pinC, HIGH);   
      digitalWrite(pinF, HIGH);   
      digitalWrite(pinG, HIGH);       
      delay(1); 
      break;
    case 5:
      digitalWrite(pinA, HIGH);   
      digitalWrite(pinC, HIGH);   
      digitalWrite(pinD, HIGH);    
      digitalWrite(pinF, HIGH);   
      digitalWrite(pinG, HIGH);       
      delay(1);
      break;
    case 6:
      digitalWrite(pinA, HIGH);      
      digitalWrite(pinC, HIGH);   
      digitalWrite(pinD, HIGH);   
      digitalWrite(pinE, HIGH);   
      digitalWrite(pinF, HIGH);   
      digitalWrite(pinG, HIGH);       
      delay(1);
      break;
    case 7:
      digitalWrite(pinA, HIGH);   
      digitalWrite(pinB, HIGH);   
      digitalWrite(pinC, HIGH);    
      delay(1); 
      break;
    case 8:
      digitalWrite(pinA, HIGH);   
      digitalWrite(pinB, HIGH);   
      digitalWrite(pinC, HIGH);   
      digitalWrite(pinD, HIGH);   
      digitalWrite(pinE, HIGH);   
      digitalWrite(pinF, HIGH);   
      digitalWrite(pinG, HIGH);       
      delay(1);
      break;
    case 9:
      digitalWrite(pinA, HIGH);   
      digitalWrite(pinB, HIGH);   
      digitalWrite(pinC, HIGH);   
      digitalWrite(pinD, HIGH);   
      digitalWrite(pinF, HIGH);   
      digitalWrite(pinG, HIGH);       
      delay(1); 
      break;
    case 10:
      // c if you want to display degrees celcius
      digitalWrite(pinA, HIGH);   
      digitalWrite(pinF, HIGH);   
      digitalWrite(pinG, HIGH);       
      delay(1); 
      break;
    case 11:
      // F if you want to show degrees fahrenheit
      digitalWrite(pinA, HIGH);
      digitalWrite(pinE, HIGH);
      digitalWrite(pinF, HIGH);
      digitalWrite(pinG, HIGH);
      delay(1);
      break;
    case 12:
      //-
      digitalWrite(pinG, HIGH);
      delay(1);
      break;
    default:
      delay(1);
      break;
  }
}

void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
}
