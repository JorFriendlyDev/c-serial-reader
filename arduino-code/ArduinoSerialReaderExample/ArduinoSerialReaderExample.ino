bool stringComplete = false;
String inputString = ""; 
String commandString = ""; 

const int led1 = A0;
const int led2 = A1;

void setup() {
  Serial.begin(9600);
}

void loop() {
  // Main way of using commands
  if(stringComplete == true)
  {
    stringComplete = false;
    getCommand();
    
    if(commandString.equals("LED1"))
    {
        ChangeLedState(led1, std::stoi(inputString));
    }
    else if(commandString.equals("LED2"))
    {
        ChangeLedState(led2, std::stoi(inputString));
    }
  }
}

// main way of getting command, commands are always 4 caracters long.
void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
}

void getCommand()
{
  if(inputString.length()>0)
  {
    commandString = inputString.substring(1,5);
  }
}


// example code to turn led's on and of
void ChangeLedState(int pin, int value)
{
  analogWrite(pin, value);
}
