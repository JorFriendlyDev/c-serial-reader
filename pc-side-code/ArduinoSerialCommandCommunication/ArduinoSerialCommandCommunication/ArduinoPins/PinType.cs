﻿namespace ArduinoSerialCommandCommunication.ArduinoPins
{
    /// <summary>
    /// port/pin types of the arduino, when Unknown is used it assumes digital and will behave likewise
    /// </summary>
    public enum PinType
    {
        Digital,
        Analog,
        Unknown
    }
}
