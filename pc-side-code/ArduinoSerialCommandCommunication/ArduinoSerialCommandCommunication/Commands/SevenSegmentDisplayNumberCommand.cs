﻿using ArduinoSerialCommandCommunication.SevenSegmentDisplays;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArduinoSerialCommandCommunication.Commands
{
    /// <summary>
    /// command of type <see cref="Command"/> that handles seven segment display commands
    /// </summary>
    public class SevenSegmentDisplayNumberCommand : Command
    {
        /// <summary>
        /// Creates new <see cref="SevenSegmentDisplayNumberCommand"/> that generates the serialText for the command
        /// </summary>
        /// <param name="ssdId"></param>
        /// <param name="digitCount"></param>
        /// <param name="value">If amount of digit after the decimal point of vlaue exceeds the digitCount it will be trimed accordingly</param>
        public SevenSegmentDisplayNumberCommand(string ssdId, DigitCount digitCount, double value)
            :base("#SSD")
        {
            SerialText = BaseCommand + ssdId + TrimValue((int)digitCount,value);
        }

        /// <summary>
        /// trims double to match the max number of digits
        /// </summary>
        private string TrimValue(int digits, double value)
        {
            var lenght = Math.Truncate(value).ToString().Length;

            if (lenght > digits)
            {
                throw new ArgumentException("The value was to big for the seven segment display with the selected digitCount");
            }

            int decimals = digits - lenght;
            int pow = (int)Math.Pow(10, decimals);

            return (Math.Truncate(value * pow) / pow).ToString();
        }
    }
}
