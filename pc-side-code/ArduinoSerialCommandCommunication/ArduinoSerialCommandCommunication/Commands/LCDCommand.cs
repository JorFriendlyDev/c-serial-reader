﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArduinoSerialCommandCommunication.Commands
{
    /// <summary>
    /// This command of type <see cref="Command"/> handels displaying text on an LCD
    /// </summary>
    public class LCDCommand : Command
    {
        /// <summary>
        /// creates a new LCDCommand that generates a serial text
        /// </summary>
        /// <param name="lcdId"></param>
        /// <param name="value"></param>
        public LCDCommand(string lcdId, string value)
            :base("#LCD")
        {
            SerialText = BaseCommand + lcdId + value;
        }
    }
}
