﻿using System;
using ArduinoSerialCommandCommunication.ArduinoPins;

namespace ArduinoSerialCommandCommunication.Commands
{
    /// <summary>
    /// This command of type <see cref="Command"/> handels turning a led on/of
    /// </summary>
    public class LedCommand : Command
    {
        /// <summary>
        /// creates a new LedCommand
        /// </summary>
        /// <param name="ledId">Id of the led known to the arduino</param>
        /// <param name="portType"><see cref="PortType"/> of the Arduino port the led is attached to</param>
        /// <param name="value">a number between 1 and 255</param>
        public LedCommand(string ledId, PinType portType, int value)
            :base("#LED")
        {
            switch (portType)
            {
                case PinType.Digital:
                case PinType.Unknown:
                    double d = value / 255;
                    int i = (int)(Math.Round(d)) * 255;
                    SerialText = BaseCommand + ledId + i.ToString();
                    break;
                case PinType.Analog:
                    SerialText = BaseCommand + ledId + value.ToString();
                    break;
            }
        }
    }
}
