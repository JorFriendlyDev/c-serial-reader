﻿using System.Windows.Input;

namespace ArduinoSerialCommandCommunication.Commands
{
    /// <summary>
    /// Command base class
    /// </summary>
    public abstract class Command 
    {
        internal string BaseCommand { get; }
        public Command(string baseCommand)
        {
            this.BaseCommand = baseCommand;
        }
        /// <summary>
        /// The serial command that can be send over the connected serial port, The command will be auto generated based on the command type you send
        /// </summary>
        public string SerialText { get; internal set; }
    }
}
