﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArduinoSerialCommandCommunicationCore.Commands
{
    /// <summary>
    /// This command of type <see cref="Command"/> handels displaying text on an LCD
    /// </summary>
    public class LcdCommand : Command
    {
        /// <summary>
        /// creates a new LCDCommand that generates a serial text
        /// </summary>
        /// <param name="lcdId"></param>
        /// <param name="value"></param>
        public LcdCommand(string lcdId, string value)
            : base("#LCD")
        {
            SerialText = BaseCommand + lcdId + value;
        }
    }
}
