﻿namespace ArduinoSerialCommandCommunicationCore.SevenSegmentDisplays
{
    public enum DigitCount
    {
        One = 1,
        Two = 2,
        Three = 3,
        Four = 4
    }
}
