﻿using ArduinoSerialCommandCommunicationCore.Commands;
using System;
using System.IO.Ports;

namespace ArduinoSerialCommandCommunicationCore
{
    public class Arduino
    {
        private SerialPort _port;

        /// <summary>
        /// Main entrance to create a new Arduino class
        /// </summary>
        /// <param name="portName"> The name of the <see cref="SerialPort"/> you want to use</param>
        public Arduino(string portName)
        {
            connect(portName);
        }

        /// <summary>
        /// Methode thats connects your <see cref="Arduino"/> class to a serial port
        /// </summary>
        /// <param name="selectedPort">The name of the <see cref="SerialPort"/> you want to connect to</param>
        public void connect(string portName)
        {
            try
            {
                _port = new SerialPort(portName, 9600, Parity.None, 8, StopBits.One);
                if (!_port.IsOpen)
                {
                    _port.Open();
                    Console.WriteLine("Connected");
                }
                else
                {
                    Console.WriteLine("There was already an open port");
                }
            }
            catch (Exception e)
            {
                throw new Exception($"No connection could be made: {e.Message}");
            }
        }

        /// <summary>
        /// Disconnects your <see cref="Arduino"/> class from the <see cref="SerialPort"/> to wich it is connected
        /// </summary>
        public void disconnect()
        {
            if (_port.IsOpen)
            {
                _port.DiscardInBuffer();
                _port.DiscardOutBuffer();
                _port.Close();
            }
        }

        /// <summary>
        /// Send a <typeparamref name="TCommand"/> of type <see cref="Command"/> to the Arduino
        /// </summary>
        /// <typeparam name="TCommand"></typeparam>
        /// <param name="command">A Command of type <see cref="Command"/></param>
        public void SendCommand<TCommand>(TCommand command) where TCommand : Command
        {
            if (_port.IsOpen)
            {
                _port.WriteLine(command.SerialText);
            }
            else
            {
                throw new Exception("There is now SerialPort Connection so no command has been send");
            }
        }

        /// <summary>
        /// Reads and returns the to this point know serial communication of the connected <see cref="SerialPort"/>
        /// </summary>
        /// <returns>serial communication data</returns>
        public string ReadSerial()
        {
            if (_port.IsOpen)
            {
                return _port.ReadExisting();
            }
            else
            {
                return "Not connected to arduino";
            }
        }
    }
}
