# C Serial reader

I created this code when I started programming, I had some arduinos laying arround from my brother. I wanted to control the board with c# on my pc. This code helped me with the serial comunication. The code isn't perfect in any way and is created a couple of years ago. I will update it in the future and fix some mistakes 

Use it as you like, change it if you want.

# How to use this code
There are two folder in this project one is pc-side-code, the other is arduino-side-code. These folders contain both vs files and arduikno IDE files.

# Note
When I have some free time I will expand the Arduino base class and the Command classes, Furthermore I will add a proper readme later.

# Planned additions
 - A arduino library that can be imported to use with the c# library
 - Because thr project is a bit older and is .Net framework I will port the code and add mutiple target frameworks
 - Add the .net package to nuget and create a pipeline accordingly 
 - Add example project for LCD, 4-digit 7-segment, and led variations
 - Provide breadboard schema's for every example project
 - Provide documentation for the C# package and the extensibility (Creating your own commands and using them from the arduino class)
 - Provide ArduinoCLI tool as example project that implements the package

# Optional additions
 - Adding component classes instead of string Id's for instance a LCD class with current state, id etc. You will be able to add them to a list Arduino.LCDs
	once in this list you can easly use methodes like SetText(), Clear() etc. 
